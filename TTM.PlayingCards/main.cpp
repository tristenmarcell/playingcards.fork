#include <iostream>
#include <conio.h>
#include <string>

enum Suit {
	HEARTS,
	DIAMONDS,
	SPADES,
	CLUBS
};

enum Rank {
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	Six = 6,
	Seven = 7,
	Eight = 8,
	Nine = 9,
	Ten = 10,
	Jack = 11,
	Queen = 12,
	King = 13, 
	Ace = 14
};

struct Card {
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) {
	std::cout << "The ";
	
	if (card.rank == 11) {
		std::cout << "Jack";
	}
	else if (card.rank == 12) {
		std::cout << "Queen";
	}
	else if (card.rank == 13) {
		std::cout << "King";
	}
	else if (card.rank == 14) {
		std::cout << "Ace";
	}
	else {
		std::cout << card.rank;
	};
	
	std::cout << " of ";
	
	if (card.suit == 0) {
		std::cout << "Hearts.\n";
	}
	else if (card.suit == 1) {
		std::cout << "Diamonds.\n";
	}
	else if (card.suit == 2) {
		std::cout << "Spades.\n";
	}
	else if (card.suit == 3) {
		std::cout << "Clubs.\n";
	}
};

Card HighCard(Card card1, Card card2) {
	if (card1.rank >= card2.rank) {
		return card1;
	}
	else {
		return card2;
	}
};

int main() {

	Card card1;
	card1.rank = Three;
	card1.suit = SPADES;

	Card card2;
	card2.rank = Ten;
	card2.suit = HEARTS;

	PrintCard(HighCard(card1, card2));

	_getch();
	return 0;
};